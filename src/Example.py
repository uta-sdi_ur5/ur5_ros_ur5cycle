#!/usr/bin/env python2

import rospy,sys,numpy as np
#import moveit_commander
from copy import deepcopy
from geometry_msgs.msg import Twist
import moveit_msgs.msg
from std_msgs.msg import Header
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from time import sleep

class ur5_test:
    def __init__(self):
        rospy.init_node("ur5_test",anonymous=False)
        self.points=[]
        # subscribe to the command topic
        self.jointTrajectory_pub = rospy.Publisher('/arm_controller/command',JointTrajectory,queue_size = 1)
        
        self.Idle_joints= [0,0,0,0,0,0]

        self.trajectory = JointTrajectory()

        self.trajectory.points.positions = [1,1,1,0,0,0]
        self.trajectory.points.velocities = [0.1,0.1,0.1,0.1,0.1,0.1] 
        self.trajectory.points.accelerations = [1,1,1,1,1,1]
        self.trajectory.points.effort = [1,1,1,1,1,1] 
        rospy.spin()


a = ur5_test()